package apiAutomation_Utils;


public class URLGenerator {

    public String getMethod = "/students";
    public String addMethod = "/students";
    public String updateMethod = "/student";
    public String deleteMethod = "/students";

    URLGenerator(String baseUrl) {
        getMethod = baseUrl + getMethod;
        addMethod = baseUrl + addMethod;
        updateMethod = baseUrl + updateMethod;
        deleteMethod = baseUrl + deleteMethod;
    }

}
