package apiAutomation_Utils;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.Map;

public class RESTUtils {

    public Response get(String url) {
        Response response = RestAssured.given()
                .header("Authorization", "Bearer")
                .header("Content-Type", "application/json")
                .log().all()
                .get(url);
        return response;
    }


    public Response getWithParam(String url, String key, int id) {
        Response response = RestAssured.given()
                .param(key, id)
                .log().all()
                .get(url);
        return response;
    }

    public Response getWithMultipleParams(String url, Map<String, Integer> map) {
        Response response = RestAssured.given()
                .queryParams(map)
                .log().all()
                .get(url);
        return response;
    }

    public Response post(String url, String payload) {
        Response response = RestAssured.given()
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .log().all()
                .body(payload)
                .post(url);
        return response;
    }

    public Response put(String url, String payload) {
        Response response = RestAssured.given()
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .log().all()
                .body(payload)
                .put(url);

        return response;
    }

    public Response delete(String url) {
        Response response = RestAssured.given()
                .log().all()
                .delete(url);
        return response;
    }

    public Response deleteWithParams(String url, Map<String ,Integer>map) {
        Response response = RestAssured.given()
                .queryParams(map)
                .log().all()
                .delete(url);
        return response;
    }
}
