package apiAutomation_POJO;

import com.google.gson.Gson;

import java.util.Map;

public class Students {
    private String firstName;
    private String lastName;
    private String degree;
    private String department;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String createStudentPayload(Map<String, String> testData) {
        Students students = new Students();
        students.setFirstName(testData.get("firstname"));
        students.setLastName(testData.get("lastname"));
        students.setDepartment(testData.get("dept"));
        students.setDegree(testData.get("degree"));

        Gson gson = new Gson();
        return gson.toJson(students);
    }

    public String updateStudentPayload(Map<String, String> testData) {
        Students students = new Students();
        students.setFirstName(testData.get("firstname"));
        students.setLastName(testData.get("lastname"));
        Gson gson = new Gson();
        String studentDetails= gson.toJson(students);
        return studentDetails;
    }

}
