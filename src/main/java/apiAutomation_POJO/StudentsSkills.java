package apiAutomation_POJO;

public class StudentsSkills {
    private String field;
    private String programmingLanguage;
    private String interests;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }
}
