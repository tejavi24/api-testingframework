package apiAutomation_POJO;

import com.google.gson.Gson;

import java.util.Map;

public class Char {
    private String thumbail;
    private String name;
    private String location;


    public String getThumbail() {
        return thumbail;
    }

    public void setThumbail(String thumbail) {
        this.thumbail = thumbail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }



    public String createCharPayload(Map<String, String> testData) {
        Char Character = new Char();
        Character.setThumbail(testData.get("thumbail"));
        Character.setName(testData.get("name"));
        Character.setLocation(testData.get("location"));

        Gson gson = new Gson();
        return gson.toJson(Character);
    }

    public String updateCharPayload(Map<String, String> testData) {
        Char Character = new Char();
        Character.setName(testData.get("editname"));
        Character.setLocation(testData.get("editloc"));
        Gson gson = new Gson();
        String charDetails= gson.toJson(Character);
        return charDetails;
    }

}
