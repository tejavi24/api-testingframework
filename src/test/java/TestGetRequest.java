import apiAutomation_Utils.RESTUtils;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class TestGetRequest {

    @Test
    public void getStudents() {
        String url = "http://localhost:3000/students";
        RESTUtils restUtils = new RESTUtils();
        Response response = restUtils.get(url);
        response.prettyPrint();
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void getStudentsById() {
        String url = "http://localhost:3000/students/";
        String key = "id";
        int value = 5;
        RESTUtils restUtils = new RESTUtils();
        Response response = restUtils.getWithParam(url, key, value);
        response.prettyPrint();
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void getStudentAddressByMultitudeParams() {
        String url = "http://localhost:3000/address/";
        String key = "zipcode";
        int value = 35256;
        RESTUtils restUtils = new RESTUtils();

        Map<String, Integer> map = new HashMap<>();
        map.put(key, value);
        Response response = restUtils.getWithMultipleParams(url, map);
        response.prettyPrint();
        Assert.assertEquals(response.statusCode(), 200);
    }

}
