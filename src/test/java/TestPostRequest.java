import apiAutomation_POJO.Char;
import apiAutomation_POJO.Students;
import apiAutomation_Utils.RESTUtils;
import com.github.javafaker.Faker;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class TestPostRequest {

    @Test
    public void createNewStudentRecord() {
        String url = "http://localhost:3000/students/";

        RESTUtils restUtils = new RESTUtils();
        Students students = new Students();

        Faker faker=new Faker();
        String firstname=faker.name().firstName();
        String lastname=faker.name().lastName();

        Map<String, String> testData = new HashMap<>();
        testData.put("firstname", firstname);
        testData.put("lastname", lastname);
        testData.put("dept", "Data science");
        testData.put("degree", "Diploma");

        String payload = students.createStudentPayload(testData);

        Response response = restUtils.post(url, payload);
        response.prettyPrint();
        Assert.assertEquals(response.statusCode(), 201);
    }


    @Test
    public void createNewCharacter() {
        String postURL = "https://restool-sample-app.herokuapp.com/api/character";

        RESTUtils restUtils = new RESTUtils();
        Char Character = new Char();

        Faker faker=new Faker();

        String thumb="test-thumbnail";
        String name=faker.name().name();
        String loc="test location";

        Map<String, String> testData = new HashMap<>();
        testData.put("thumbnail", thumb);
        testData.put("name", name);
        testData.put("location", loc);

        String payload = Character.createCharPayload(testData);

        Response response = restUtils.post(postURL, payload);
        response.prettyPrint();
//        Assert.assertEquals(response.statusCode(), 201);
    }

}
