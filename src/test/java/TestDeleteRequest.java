import apiAutomation_Utils.RESTUtils;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class TestDeleteRequest {
    @Test
    public void deleteStudentById() {
        String url = "https://restool-sample-app.herokuapp.com/api/character/3YtTJL5HgN0i";

        String key = "id";
        String value = "bFcuqCdqBLyU";
        RESTUtils restUtils = new RESTUtils();
        Response response = restUtils.delete(url);
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void deleteStudentByParams() {
        String url = "http://localhost:3000/students/";

        String key = "id";
        int value = 1;
        Map<String, Integer> testData = new HashMap<>();
        testData.put(key, value);
        RESTUtils restUtils = new RESTUtils();
        Response response = restUtils.deleteWithParams(url, testData);
        Assert.assertEquals(response.statusCode(), 200);
    }
}

