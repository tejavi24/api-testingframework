package config;

import java.io.*;
import java.util.Properties;

public class ReadPropertiesFile {
    static Properties prop = new Properties();
    static String path = "/Users/tejaswini/Desktop/api-testingframework/src/test/java/config/config.properties";

    public static void main(String[] args) {
       loadProperties();
       setProperties();
    }

    public static void loadProperties() {
        try {
            InputStream inputFile = new FileInputStream(path);
            prop.load(inputFile);
            String url = prop.getProperty("baseUrl");
            System.out.println(url);

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            e.printStackTrace();
        } catch (IOException exp) {
            exp.printStackTrace();
        }
    }

    public static void setProperties() {

        try {
            OutputStream outputFile = new FileOutputStream(path);
            prop.setProperty("accessToken", "token223");
            prop.store(outputFile, "updated");

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
