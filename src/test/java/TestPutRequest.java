import apiAutomation_POJO.Char;
import apiAutomation_POJO.Students;
import apiAutomation_Utils.RESTUtils;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class TestPutRequest {

    @Test
    public void updateStudentRecord() {
        String url = "http://localhost:3000/student/";

        RESTUtils restUtils = new RESTUtils();
        Students students = new Students();



        Map<String, String> testData = new HashMap<>();
        testData.put("firstname", "Danis");

        String payload = students.updateStudentPayload(testData);

        Response response = restUtils.put(url, payload);
        response.prettyPrint();
        Assert.assertEquals(response.statusCode(),200);
    }

    @Test
    public void updateCharRecord() {
        String editURL = "https://restool-sample-app.herokuapp.com/api/character/11kCIFByOQtx";

        RESTUtils restUtils = new RESTUtils();
        Char Character = new Char();

        Map<String, String> testData = new HashMap<>();
        testData.put("location", "EDITME");

        String payload = Character.updateCharPayload(testData);

        Response response = restUtils.put(editURL, payload);
        response.prettyPrint();
        Assert.assertEquals(response.statusCode(),200);
    }

}
